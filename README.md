# php-bitfield-model

#### 介绍
使用位域参与数据库存储的模型操作

#### 软件架构
依赖php-bitfield位域操作包，基于webman框架

#### 思想
* 使用场景：将位域串存于mysql某记录的某一列中(位置唯一)，一般可存储数千条关联记录。如某用户某本书的章节解锁记录，这样可以节约用户-章节解锁记录表数千倍的记录数，大大节约了空间和查询时间。
* 位域结构：位域串使用标准的行-列结构存储记录，与MySQL结构类似。

#### 安装教程

1.  composer require bdhert/php-bitfield-model
2.  开发版：composer require bdhert/php-bitfield-model:"dev-master"
3.  基础版：composer require bdhert/php-bitfield-model:"^0.1"

#### 使用说明

```php

<?php
use bdhert\PhpBitfieldModel\Model;
use bdhert\PhpBitfieldModel\exception\BitFieldModelException;

class UserCallback extends Model {
    protected array $fields = ['test1' => 1, 'test2' => 1];   // 定义字段，最多14列
    protected string $source_table = 'lv_operate_book_users'; // 数据源所在表名
    protected string $source_field = 'callback';              // 存储数据源的字段名
}

try {
    UserCallback::query(['id' => 1])->insert(['test1' => 5, 'test2' => 11])
    var_dump(UserCallback::where('test1', 5)->get());
} catch (BitFieldModelException $e) {
    throw new BitFieldModelException($e->getMessage(), 400);
}

```
